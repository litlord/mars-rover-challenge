using System;

namespace Mars_Rover_Challenge
{
    public class Rover
    {
        public int x_position{get;set;}
        public int y_position{get;set;}
        public int orientation{get;set;}
    }
}