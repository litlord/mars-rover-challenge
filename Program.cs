﻿using System;

namespace Mars_Rover_Challenge
{
    class Program
    {
        public static void Main(string[] args)
        {
            Rover rover = new Rover();
            Console.WriteLine("Enter Current Zone with space between coordinates:  ");
            String txt_current_zone = Console.ReadLine().ToString();
            if (txt_current_zone.Length >= 2)
            {
                String[] current_zone = txt_current_zone.Split(' ');
                int zone_x_pos = Convert.ToInt32(current_zone[0]);
                int zone_y_pos = Convert.ToInt32(current_zone[1]);
                //get user to input rover details
                Console.WriteLine("Enter starting coordinates & orientation of Rover: ");
                String txt_rover_pos = Console.ReadLine();

                if (txt_rover_pos.Replace(" ", "").Length >= 3)
                {
                    String[] rover_pos = txt_rover_pos.Split(" ");
                    rover.x_position = Convert.ToInt32(rover_pos[0]);
                    rover.y_position = Convert.ToInt32(rover_pos[1]);
                    rover.orientation = setOrientation(rover_pos[2]);
                    
                    //get user to input rover commands
                    bool commands_done = false;
                    while(!commands_done){
                        Console.WriteLine("Enter Command String (N to complete): ");
                        String txt_rover_commands = Console.ReadLine();
                        //execute commands thru parser
                        CommandParser commandParser = new CommandParser();
                        rover = commandParser.processCommand(txt_rover_commands, rover); 
                        Console.WriteLine("Rover Location: " + rover.x_position + " " + rover.y_position + " " + displayOrientation(rover.orientation));
                        //check if the rover is still in the zone after command is executed
                        if(rover.x_position < 0 || rover.x_position > zone_x_pos || rover.y_position < 0 || rover.y_position > zone_y_pos){
                            Console.WriteLine("Eish! Your rover fell out of the safe zone. Please send a new Billion dollar rover :(");
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Inputted coordinates are not in correct format");
                }
            }
            else
            {
                Console.WriteLine("Inputted Zone coordinates are not in correct format");
            }
        }

        private static int setOrientation(String orientation_input)
        {
            String txt_orientation = orientation_input.ToString().ToLower();
            if (txt_orientation.Equals("n"))
                return 0;
            else if (txt_orientation.Equals("e"))
                return 90;
            else if (txt_orientation.Equals("s"))
                return 180;
            else if (txt_orientation.Equals("w"))
                return 270;
            else
                return -1;
        }    
        private static String displayOrientation(int orientation_val)
        {
            if (orientation_val == 0)
                return "N";
            else if (orientation_val == 90)
                return "E";
            else if (orientation_val == 180)
                return "S";
            else if (orientation_val == 270)
                return "W";
            else
                return null;
        }
    }
}
