using System;

namespace Mars_Rover_Challenge
{
    public class CommandParser
    {
        public Rover processCommand(String command_string, Rover current_rover_pos)
        {
            //verify command string
            if (command_string != null && command_string.Length > 0)
            {
                if (verifyCommand(command_string))
                {
                    Rover updated_rover = current_rover_pos;
                    command_string = command_string.ToLower();
                    foreach (char command in command_string)
                    {
                            if (command.Equals('m'))
                            {
                                if (updated_rover.orientation == 0) 
                                {
                                    updated_rover.y_position += 1;
                                }
                                else if(updated_rover.orientation == 180){
                                    updated_rover.y_position -= 1;
                                }
                                else if (updated_rover.orientation == 90) 
                                {
                                    updated_rover.x_position += 1;
                                }
                                else if(updated_rover.orientation == 270){
                                    updated_rover.x_position -= 1;
                                }
                            }
                            else if (command.Equals('r'))
                            {
                                updated_rover.orientation = rotateRover(90, updated_rover.orientation);
                            }
                            else if (command.Equals('l'))
                            {
                                updated_rover.orientation = rotateRover(-90, updated_rover.orientation);
                            }
                    }
                    return updated_rover;
                }
                else{
                    Console.WriteLine("invalid command in list of commands");
                    return null;
                }
            }
            else
            {
                Console.WriteLine("list of commands not entered");
                return null;
            }
        }
        private Boolean verifyCommand(String command_string)
        {
            bool all_commands_valid = true;
            command_string = command_string.ToLower();
            //String[] commands = command_string.Split();
            foreach (char command in command_string)
            {
                if (!command.Equals('m'))
                {
                    if (!command.Equals('l'))
                    {
                        if (!command.Equals('r'))
                        {
                            return false;
                        }
                    }
                }
            }
            return all_commands_valid;
        }
        /*
        * 0 = North
        * 90 = East
        * 180 = South
        * 270 = West
        */

        private int rotateRover(int rotation, int current_orientation){
            int new_orientation = 0;
            if(current_orientation == 270 && rotation == 90){
                new_orientation = 0;
            }
            else if(current_orientation == 0 && rotation == -90){
                new_orientation = 270;
            }
            else{
                new_orientation = current_orientation + rotation;
            }
            return new_orientation;
        }
    }
}