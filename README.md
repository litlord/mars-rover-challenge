Mars Rover Challenge 

1. DESCRIPTION OF SOLUTION
The program users a rover object(Rover.cs) that stores the Cartesian coordinates and orientation to model the Rover and its movements on the surface.
The program accepts commands to direct the Rovers movements via the console. These commands are processed using a parser class(CommandPaerser.cs) which validates that the commands are valid. The updated position of the rover is then displayed after the commands have been processed.

2. HOW TO USE
-When program is ran, you will be prompted to enter the coordinates of the allowable zone the rover may move in.
-The corordinates should be inputted in a single line with a space between the x and y coordinates. (eg. 10 9)
-you will then be prompted to enter the coordinates and orientation of the rovers starting position. 
-The cordinates and orientation of the rover should be inputted in a single line with a space between them. (eg 2 1 E)
-You will finnally be prompted to input the movement commands of the rover.(eg. MMLRMM) These willl continuously requested until you exit the program by entering 'q'.

3. EXPLANATION OF SOLUTION
-TO accurately model how the rover moves, the rovers orientation is converted from the charecters such as 'S' and 'N' to values that can be used for computation.
-These conversions are detailed below:
North = 'N' = 0
East = 'E' = 90
South = 'S' = 180
West = 'W' = 270